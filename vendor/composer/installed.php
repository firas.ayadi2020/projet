<?php return array(
    'root' => array(
        'pretty_version' => 'v7.0.99',
        'version' => '7.0.99.0',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'symfony/skeleton',
        'dev' => true,
    ),
    'versions' => array(
        'symfony/flex' => array(
            'pretty_version' => 'v2.4.3',
            'version' => '2.4.3.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../symfony/flex',
            'aliases' => array(),
            'reference' => '6b44ac75c7f07f48159ec36c2d21ef8cf48a21b1',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-iconv' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php72' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php73' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php74' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php80' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php81' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/polyfill-php82' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '*',
            ),
        ),
        'symfony/skeleton' => array(
            'pretty_version' => 'v7.0.99',
            'version' => '7.0.99.0',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
